const express = require('express');
const bodyParser = require('body-parser');

port = process.env.PORT || 8000;
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
  });
app.listen(port, function () {
    console.log('Express is running on port '+ port);
});

var routes = require('./apps/routes/appRouter'); //importing route

routes(app); //register the route