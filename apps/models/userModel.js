// create user model
const Sequelize = require("sequelize");
var sequelize = require("../config/db");
const bcrypt = require("bcrypt");
const User = sequelize.define(
  "User",
  {
    user_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    first_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    last_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    id_tipouser: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    status: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 1
    }
  },
  {
    hooks: {
      beforeCreate: User => {
        {
          User.password =
            User.password && User.password != ""
              ? bcrypt.hashSync(User.password, 10)
              : "";
        }
      }
    }
  }
);
// create table with user model
User.sync()
  .then()
  .catch();

// defaultScope: {
//     attributes: { exclude: ['password','token'] },
//   }

module.exports = User;
