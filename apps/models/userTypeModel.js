const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const userType = sequelize.define('tipo_usuario', {
    id_tipouser: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name:{
        type: Sequelize.STRING,
        allowNull: false,
    }
});
// create table with user model
userType.sync()
.then()
.catch();

module.exports = userType;