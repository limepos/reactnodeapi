const Sequelize = require('sequelize');
var sequelize = require('../config/db');

const ticket = sequelize.define('ticket', {
    ticket_id: {
        type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
    },
    user_id:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    ticket:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    ticket_status:{
        type: Sequelize.STRING,
        allowNull: false,
    }
});
// create table with user model
ticket.sync()
.then()
.catch();

module.exports = ticket;