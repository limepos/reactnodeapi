const { Op } = require("sequelize");
var User = require("../models/userModel");
var Ticket = require("../models/ticketModel");
var owner = require("./getOwnerIdController");



  exports.getAllTickets = async () => {
 
    User.hasMany(Ticket,{foreignKey : 'user_id'});
    Ticket.belongsTo(User,{foreignKey: 'user_id'});
    return await Ticket.findAll({where: { [Op.or]: [{ticket_status: 1 },{ ticket_status: 2 }]}, include: [User]});

  };

  exports.addNewTicket = async (ticketdata, author) => {
      ticketdata.ticket_status = 1;
      await  Ticket.create(ticketdata).then(async t => {
          return t;
    });
};  
  
exports.getAllTicketsByUser =  async (params, author) => {
    Ticket.belongsTo(User,{foreignKey: 'user_id'});
    User.hasMany(Ticket,{foreignKey : 'user_id'});
    return await Ticket.findAll({include: { model : User }, where: { [Op.or]: [{ticket_status: 1 },{ ticket_status: 2 }], user_id: params.userId }});

  };

  exports.getCount = async () => {
    return await Ticket.findAll({
      attributes: [[Sequelize.fn("COUNT", Sequelize.col("ticket_id")), "count"]]
    });
  };