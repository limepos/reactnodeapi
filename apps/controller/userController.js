var User = require("../models/userModel");
var Ticket = require("../models/ticketModel");
var UserType = require("../models/userTypeModel");
var owner = require("./getOwnerIdController");
const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");

exports.createUser = async userdata => {
    await User.create(userdata).then(async user => {
        
          return user;

  });
};

exports.getAllUsers = () => {
  return  User.findAll({
      where: {
        id_tipouser : 2,
        status : 1
      }
   
  });
};
exports.getUser = async obj => {
  return await User.findOne({
    where: obj
  });
};

exports.getProfile = async author => {
  return await owner.getOwnerId(author).then(async owner_id => {
    console.log("ownerID == > ", owner_id);

    return await User.findOne({
      where: {
        owner_id: owner_id
      }
    });
  });
};

exports.getCount = async () => {
  return await User.findAll({
    attributes: [[Sequelize.fn("COUNT", Sequelize.col("user_id")), "count"]]
  });
};

exports.updateUser = async (userdata, params) => {
  return owner = await User.update(userdata, {
    where: { user_id: params.userid }
  })
};

exports.deleteUser = async (params) => {
  return owner = await User.update({status : 3}, {
    where: { user_id: params.userid }
  })
};


exports.ticketUpdate = (userdata, params) => {
  return Ticket.update(userdata, {
    where: { ticket_id: params.ticketid }
  })
};