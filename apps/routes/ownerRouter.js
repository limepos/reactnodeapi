module.exports = function(app) {
  const bcrypt = require("bcrypt");
  var Useraction = require("../controller/userController");
  var Ticketaction = require("../controller/ticketController");
  const jwt = require("jsonwebtoken");
  const passport = require("passport");
  const passportJWT = require("passport-jwt");
  let ExtractJwt = passportJWT.ExtractJwt;
  let JwtStrategy = passportJWT.Strategy;
  let jwtOptions = {};
  jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  jwtOptions.secretOrKey = "appsonsdevlopertsecrerkey@123";
  let strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
    console.log("payload received", jwt_payload);
    let user = Useraction.getUser({
      user_id: jwt_payload.user_id
    });
    if (user) {
      next(null, user);
    } else {
      next(null, false);
    }
  });

  passport.use(strategy);

  app.use(passport.initialize());

  app.get("/", function(req, res) {
    console.log(config.super());
    res.json({
      message: "Node api's Running."
    });
  });

  app.post("/register", async function(req, res, next) {
    
    let useremail = await Useraction.getUser({
      email : req.body.email
    });

    if (useremail) {
      res.status(401).json({
        message: "This email is already Register.",
        status: false
      });
    } else {
      Useraction.createUser(req.body).then(user =>
        res.status(200).json({
          message: "account created successfully",
          status: true,
          data: "user"
        })
      ).catch((e) => {
        res.status(400).json({
          message: "some thing wrong",
          status: false,
          data: e
        });
      });
    }
  });

  app.post("/login", async function(req, res, next) {
    const { email, password } = req.body;
    if (email && password) {
      let loginuser = await Useraction.getUser({
        email
      });
      if (!loginuser) {
        res.status(401).json({
          message: "No such user found",
          status: false
        });
      }
      if (
        bcrypt.compareSync(password, loginuser.password) &&
        loginuser.status == 1
      ) {
        let payload = {
          user_id: loginuser.user_id
        };
        let token = jwt.sign(payload, jwtOptions.secretOrKey);
        res.status(200).json({
          message: "ok",
          token: token,
          data: loginuser,
          status: true
        });
      } else {
        res.status(401).json({
          message: "Password is incorrect",
          status: false
        });
      }
    }
  });

  app.get("/users",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res, next) {
      Useraction.getAllUsers(req.headers).then(user =>
        res
          .status(200)
          .json({ data: user, status: true, message: "Get All users" })
      ).catch((e) => {
        res.status(400).json({
           status: false,
           message: "Some thing is wrong",
         });
      });
    }
  );

  app.get("/user/:userid",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.getUsers(req.params).then(user =>
        res.status(200).json({ data: user, status: true, message: "Get users" })
      ).catch((e) => {
        res.status(400).json({
           status: false,
           message: "Some thing is wrong",
         });
      });
    }
  );

  // Superadmin

  app.put("/user/update/:userid",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.updateUser(req.body, req.params).then(user =>
        res
          .status(200)
          .json({ data: user, status: true, message: "Updae users" })
      ).catch((e) => {
        res.status(400).json({
           status: false,
           message: "Some thing is wrong",
         });
      });
    }
  );

  app.put("/user/delete/:userid",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.deleteUser(req.params).then(user => {
        res.status(200).json({
          message: "User Delete  Successfully",
          status: true
        });
      }).catch((e) => {
        res.status(400).json({
           status: false,
           message: "Some thing is wrong",
         });
      });
    }
  );

  app.get("/user/count",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.getCount().then(user =>
        res.status(200).json({ data: user, status: true, message: "Get count" })
      ).catch((e) => {
        res.status(400).json({
           status: false,
           message: "Some thing is wrong",
         });
      });
    }
  );

  app.get("/ticket/count",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Ticketaction.getCount().then(user =>
        res.status(200).json({ data: user, status: true, message: "Get count" })
      ).catch((e) => {
        res.status(400).json({
           status: false,
           message: "Some thing is wrong",
         });
      });
    }
  );

  app.get(
    "/user",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.getProfile(req.headers).then(user =>
        res.status(200).json({
          data: user,
          status: true,
          message: "Get Owner Data Successful."
        })
      ).catch((e) => {
        res.status(400).json({
           status: false,
           message: "Some thing is wrong",
         });
      });
    }
  );

  app.put(
    "/user/update",
    passport.authenticate("jwt", {
      session: false
    }),
    async function(req, res) {
      Useraction.updateProfile(req.body, req.headers).then(user =>
        res.status(200).json({
          data: user,
          status: true,
          message: "Profile update Successful."
        })
      ).catch((e) => {
        res.status(400).json({
           status: false,
           message: "Some thing is wrong",
         });
      });
    }
  );

  app.get("/tickets",
  passport.authenticate("jwt", {
    session: false
  }),
  async function(req, res, next) {
    Ticketaction.getAllTickets(req.headers).then(user =>
      res
        .status(200)
        .json({ data: user, status: true, message: "Get All Tickets" })
    ).catch((e) => {
      res.status(400).json({
         status: false,
         message: "Some thing is wrong",
       });
    });
  }
);

app.get("/tickets/:userId",
passport.authenticate("jwt", {
  session: false
}),
async function(req, res, next) {
  Ticketaction.getAllTicketsByUser(req.params, req.headers).then(user =>
    res
      .status(200)
      .json({ data: user, status: true, message: "Get All Tickets" })
  ).catch((e) => {
    res.status(400).json({
       status: false,
       message: "Some thing is wrong",
     });
  });
}
);

app.post("/new-ticket",
  passport.authenticate("jwt", {
    session: false
  }),
  async function(req, res,next) {
    Ticketaction.addNewTicket(req.body, req.headers).then(ticket =>
      res.status(200).json({
        data: ticket,
        status: true,
        message: "Ticket creates Successful."
      })
    ).catch((e) => {
      res.status(400).json({
         status: false,
         message: "Some thing is wrong",
       });
    });
  }
); 


app.put("/ticket/update/:ticketid",
passport.authenticate("jwt", {
  session: false
}),
async function(req, res) {
  Useraction.ticketUpdate(req.body, req.params).then(user =>
    res
      .status(200)
      .json({ data: user, status: true, message: "Update Ticket" })
  ).catch((e) => {
    res.status(400).json({
       status: false,
       message: "Some thing is wrong",
     });
  });
}
);
}; // End module.
